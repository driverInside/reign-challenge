module.exports = {
  apps: [{
    name: 'app',
    script: './build/bin/www.js',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }, {
    name: 'cron',
    script: './build/cron/getPosts.js'
  }]
}
