/**
 * users.js
 *
 * Defines the users api routes
 */
'use strict'
import express from 'express'
const router = express.Router()

router.get('/', (req, res, next) => {
  res.send({
    message: 'users'
  })
})

export default router
