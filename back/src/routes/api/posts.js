/**
 * routes/api/posts.js
 *
 * Defines the posts routes.
 */
import express from 'express'
import Posts from '../../models/posts'
import PostService from '../../services/Post'
const router = express.Router()

const postService = new PostService()

router.get('/', async (req, res) => {
  const posts = await postService.getAll()
  res.send(posts)
})

router.get('/:id', async (req, res) => {
  const id = req.params.id

  const post = await postService.getById(id)

  res.send(post)
})

router.post('/', async (req, res) => {
  const postData = req.body
  const post = await postService.create(postData)
  res.send(post)
})

router.delete('/:id', async (req, res) => {
  const id = req.params.id

  await postService.removeById(id)

  res.send({
    message: `Post ${id} was deleted`
  })
})

export default router
