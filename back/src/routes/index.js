/**
 * routes/index.js
 */

import express from 'express'
import postsApi from './api/posts'

const router = express.Router()

router.use('/api/posts', postsApi)

export default router
