/**
 * cron/getPosts.js
 *
 * @description get the post from algolia
 */
import cron from 'node-cron'
import AlgoliaQry from '../libs/AlgoliaQry'
import PostService from '../services/Post'

require('../db')

const postService = new PostService()
const algoliaQry = new AlgoliaQry()

// every hour at minute 0
const time = '0 * * * *'

const init = async () => {
  const lastPost = await postService.getLast()
  const opts = {}

  if (lastPost) {
    opts.from = lastPost.createdAt
  }

  const posts = await algoliaQry.getPosts(opts)

  if (posts.length) {
    await postService.insertMany(posts)
  }
}

cron.schedule(time, init)
