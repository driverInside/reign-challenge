/**
 * services/Post.js
 */
import PostModel from '../models/posts'

const PostService = class {
  constructor () {
    this.Model = PostModel
  }

  /**
   * getAll
   * @param {integer} skip - The skip pagination arg
   * @param {integer} limit - The limit pagination arg
   * @returns {Promise} - Promise with the list of posts
   */
  async getAll (skip, limit) {
    return this.Model.find({})
      .skip(skip)
      .limit(limit)
  }

  /**
   * getById
   * @param {string} id - The objectId post id
   * @returns {Promise} - Promise with the specific post
   */
  async getById (id) {
    return this.Model.findById({ _id: id })
  }

  /**
   * create
   * @param {object} postData - The post data
   * @return {Promise} - The promise with the post created
   */
  async create (postData) {
    return this.Model.create(postData)
  }

  /**
   * insertMany
   * @param {array} posts - List of posts to insert
   * @returns {Promise} - The insertMany mongodb method
   */
  async insertMany (posts) {
    return this.Model.insertMany(posts)
  }

  /**
   * getLast
   * @returns {array|boolean} - The last post stored
   */
  async getLast () {
    const stored = await this.Model.find({})
      .sort({ createdAt: -1 })
      .limit(1)

    return stored.length ? stored[0] : false
  }

  /**
   * removeById
   * @param {string} id - The objectId id
   * @returns {Promise} - Promise that deletes a specific post
   */
  async removeById (id) {
    return this.Model.deleteOne({ _id: id })
  }
}

export default PostService
