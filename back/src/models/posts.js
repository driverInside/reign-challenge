/**
 * models/posts.js
 *
 * @description Defines the post model
 */
import mongoose from 'mongoose'

const Schema = mongoose.Schema

const PostsSchema = new Schema({
  author: { type: String },
  title: { type: String },
  storyTitle: { type: String },
  storyUrl: { type: String },
  url: { type: String },
  createdAt: { type: Date }
}, {
  versionKey: false,
  timestamps: true
})

const Posts = mongoose.model('Posts', PostsSchema, 'Posts')

export default Posts
