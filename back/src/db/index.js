/**
 * db/index.js
 * 
 * @description setup db connection
 */
import mongoose from 'mongoose'
import deb from 'debug'

const debug = deb('server:db')

const mongoHost = process.env.MONGO_HOST || 'localhost'
const mongoPort = process.env.MONGO_PORT || 27017
const mongoDB = process.env.MONGO_DB || 'posts'
const mongoUser = process.env.MONGO_USER || false
const mongoPass = process.env.MONGO_PASS || false

const mongoURI = process.env.MONGO_URI || `
  mongodb://${mongoHost}:${mongoPort}/${mongoDB}
`

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true
}

if (mongoUser && mongoPass ) {
  options.user = mongoUser
  options.pass = mongoPass
}

mongoose.connect(mongoURI, options)

const connection = mongoose.connection
connection.on('error', console.error.bind(console, 'connection error: '))
connection.once('open', () => {
  debug(`DB connected: url -> ${mongoURI}`)
})
export default connection
