/**
 * lib/AlgoliaQry.js
 *
 */
import axios from 'axios'

const url = process.env.POSTS_URL || 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs'

const AlgoliaQry = class {
  /**
   * getPosts
   * @param {Object} params - The params
   * @param {Date} Object.from - The last post date
   * @returns {Array} - The list of post to insert to db
   */
  async getPosts ({ from }) {
    const posts = await axios({
      method: 'GET',
      url
    })
      .then(response => {
        const postsDownloaded = response.data.hits
          .map(post => {
            return {
              createdAt: post.created_at,
              author: post.author,
              title: post.title,
              storyTitle: post.story_title,
              storyUrl: post.story_url,
              url: post.url
            }
          })
          .sort((a, b) => {
            const aDate = new Date(a.created_at)
            const bDate = new Date(b.created_at)

            return bDate - aDate
          })
          .filter(post => post.storyTitle || post.title)

        if (from) {
          const fromDate = new Date(from)

          return postsDownloaded.filter(post => {
            const postDate = new Date(post.createdAt)

            return fromDate < postDate
          })
        }

        return postsDownloaded
      })

    return posts
  }
}

export default AlgoliaQry
