/**
 * algolaQry.test.js
 */
import AlgoliaQry from '../lib/AlgoliaQry'

const algoliaQry = new AlgoliaQry()

/* global describe it expect */
describe('Algolia post extractor', () => {
  it('The method getPosts should exists', async () => {
    expect(algoliaQry.getPosts).toBeDefined()
  })

  it('should return an array of posts', async () => {
    const res = await algoliaQry.getPosts()
    expect(res.length).toBeGreaterThan(0)
  })

  it('The posts should be sorted by date', async () => {
    const posts = await algoliaQry.getPosts()

    for (let i = 1; i < posts.length; i++) {
      const currentDate = new Date(posts[i].created_at)
      const prevDate = new Date(posts[i - 1].created_at)

      expect(prevDate - currentDate).toBeGreaterThan(0)
    }
  })

  it('The posts should have title, author and url', async () => {
    const posts = await algoliaQry.getPosts()

    posts.forEach(post => {
      expect(post).toHaveProperty('author')
      expect(post).toHaveProperty('createdAt')
      expect(post).toHaveProperty('title')
      expect(post).toHaveProperty('storyTitle')
      expect(post).toHaveProperty('storyUrl')
      expect(post).toHaveProperty('url')
    })
  })
})
