/**
 * api/posts.test.js
 */
import faker from 'faker'
import request from 'supertest'
import app from '../../app'
import PostService from '../../services/Post'
import { connectDB, closeDatabase } from '../db'

const postService = new PostService()

/* global describe it expect beforeAll afterAll */
describe('Posts api', () => {
  beforeAll(async () => connectDB())
  afterAll(async () => closeDatabase())
  describe('Posts GET', () => {
    beforeAll(async () => {
      [...Array(10).keys()].forEach(async () => {
        await postService.create({
          author: faker.internet.userName(),
          title: faker.lorem.sentence(),
          storyTitle: faker.lorem.sentence(),
          storyUrl: faker.internet.url(),
          url: faker.internet.url(),
          createdAt: faker.date.past()
        })
      })
    })

    it('The get route should exists', async () => {
      const res = await request(app)
        .get('/api/posts')
      expect(res.statusCode).toBe(200)
    })

    it('should get an array of posts', async () => {
      const res = await request(app)
        .get('/api/posts')

      expect(res.statusCode).toBe(200)
      expect(res.body.length).toBeGreaterThan(0)

      res.body.forEach(post => {
        expect(post).toHaveProperty('author')
        expect(post).toHaveProperty('title')
        expect(post).toHaveProperty('createdAt')
        expect(post).toHaveProperty('storyTitle')
        expect(post).toHaveProperty('storyUrl')
        expect(post).toHaveProperty('url')
      })
    })
  })

  describe('Posts DELETE', () => {
    it('should delete an specific post', async () => {
      const newPost = await postService.create({
        author: faker.internet.userName(),
        title: faker.lorem.sentence(),
        storyTitle: faker.lorem.sentence(),
        storyUrl: faker.internet.url(),
        url: faker.internet.url(),
        createdAt: faker.date.past()
      })

      const id = newPost._id
      const res = await request(app)
        .delete(`/api/posts/${id}`)
      expect(res.statusCode).toBe(200)

      const postDeleted = await postService.getById(id)
      expect(postDeleted).toBeNull()
    })
  })
})
