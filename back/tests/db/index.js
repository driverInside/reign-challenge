/**
 * tests/db/index.js
 * @description setup memory db
 */
import mongoose from 'mongoose'
import MongoMemoryServer from 'mongodb-memory-server'

const mongod = new MongoMemoryServer()

/**
 * connect
 * @description Connect to memory db server
 */
const connectDB = async () => {
  const uri = await mongod.getConnectionString()

  const opts = {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }

  await mongoose.connect(uri, opts)
}

/**
 * closeDatabase
 * @description Deletes the database, close and
 *              stop the connection
 */
const closeDatabase = async () => {
  try {
    await mongoose.connection.close()
    await mongod.stop()
  } catch (err) {
    console.error(err)
  }
}

/**
 * clearDatabase
 * @description Clear all the collections in the db
 */
const clearDatabase = async () => {
  const collections = mongoose.connection.collections

  for (const key in collections) {
    const collection = collections[key]
    await collection.deleteMany()
  }
}

export {
  connectDB,
  closeDatabase,
  clearDatabase
}
