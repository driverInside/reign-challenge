import React from 'react';
import { HomeContainer } from './containers'
import { Layout } from './components'
import 'fontsource-roboto'

function App () {
  return (
    <Layout>
      <HomeContainer />
    </Layout>
  )
}

export default App;
