import React from 'react'
import Box from '@material-ui/core/Box'
import './Header.scss'

const Header = ({
  title = '',
  subtitle = ''
}) => {
  return (
    <Box className='header'>
      <h1 className='title'>{title}</h1>
      <h2 className='subtitle'>{subtitle}</h2>
    </Box>
  )
}

export default Header
