import React from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import './Layout.scss'

const Layout = ({ children }) => {
  return (
    <main>
      <CssBaseline>
        {children}
      </CssBaseline>
    </main>
  )
}

export default Layout
