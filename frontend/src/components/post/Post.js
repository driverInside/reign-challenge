import React from 'react'
import Grid from '@material-ui/core/Grid'
import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'
import './Post.scss'

const Post = ({
  postId,
  onRemove,
  author = '',
  title = '',
  storyTitle = '',
  storyUrl = '',
  url = '',
  createdAt = ''
}) => {
  return (
    <div className='post'>
      <Grid container spacing={2}>
        <Grid item md={8} xs={12}>
          <a href={url || storyUrl}>
            <p className='title'>
              {title || storyTitle}
              <span className='author'>- {author} -</span>
            </p>
          </a>
        </Grid>
        <Grid item md={3} xs={12}>
          <p className='date'>{createdAt}</p>
        </Grid>
        <Grid item md={1} xs={12}>
          <IconButton aria-label='delete' onClick={() => onRemove(postId)}>
            <DeleteIcon fontSize='small' />
          </IconButton>
        </Grid>
      </Grid>
    </div>
  )
}

export default Post
