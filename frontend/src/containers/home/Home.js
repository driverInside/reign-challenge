import React from 'react'
import { Header } from '../../components'
import PostsContainer from '../posts'

const HomeContainer = ({ children }) => {
  return (
    <>
      <Header title='HN Feed' subtitle={'We <3 hacker news!'} />
      <PostsContainer />
      <p>Este es el container de home</p>
    </>
  )
}

export default HomeContainer
