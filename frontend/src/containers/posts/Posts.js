import React, { useEffect, useState } from 'react'
import fetch from 'unfetch'
import moment from 'moment'
import Container from '@material-ui/core/Container'
import { Post } from '../../components'

const PostsContainer = ({ children }) => {
  const [posts, setPosts] = useState([])
  useEffect(() => {
    fetch('http://localhost:3500/api/posts', {
      method: 'GET'
    })
      .then(r => r.json())
      .then(response => {
        const posts = response
          .filter(post => post.url || post.storyUrl)
          .sort((a, b) => {
            const aDate = new Date(a.createdAt)
            const bDate = new Date(b.createdAt)

            return bDate - aDate
          })
        setPosts(posts)
      })
      .catch(err => {
        console.error(err)
      })
  }, [])

  const onRemove = (id) => {
    fetch(`http://localhost:3500/api/posts/${id}`, {
      method: 'DELETE'
    })
      .then(r => r.json())
      .then(response => {
        const list = posts.filter(post => id !== post._id)
        setPosts(list)
      })
      .catch(err => {
        console.error(err)
      })
  }

  return (
    <section>
      <Container>
        {posts.map((post, i) => {
          const date = moment(new Date(post.createdAt)).fromNow()

          return <Post
            key={i}
            postId={post._id}
            title={post.title}
            storyTitle={post.storyTitle}
            storyUrl={post.storyUrl}
            url={post.url}
            createdAt={date}
            onRemove={onRemove}
          />
        })}
      </Container>
    </section>
  )
}

export default PostsContainer
