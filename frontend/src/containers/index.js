import HomeContainer from './home'
import PostsContainer from './posts'

export {
  HomeContainer,
  PostsContainer
}
